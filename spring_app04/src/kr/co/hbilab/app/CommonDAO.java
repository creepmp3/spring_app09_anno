package kr.co.hbilab.app;

import java.sql.Connection;
import java.util.ArrayList;

// OracleDAO
// SelectAll() 메서드에서 메세지 출력
// 비어있는 ArrayList 객체를 리턴
public interface CommonDAO{
    
    public Connection connect();
    
    public ArrayList<DeptDTO> selectAll();
}
