package spring_app7_collection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestMain{
    public static void main(String[] args) {
        /*
        List<String> list = new ArrayList<String>();
        
        list.add("test1");
        list.add("test2");
        list.add("test3");
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("1. 반장", "숙면중");
        map.put("2. 부반장", "젤 말리는중");

        CollectionImple ci = new CollectionImple(list, map);
        ci.printMap();
        */
        
        /*
        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
        CollectionImple ci = factory.getBean("collection", CollectionImple.class);
        */
        
        ApplicationContext context = new ClassPathXmlApplicationContext("app.xml");
        CollectionInter ci = context.getBean("ci", CollectionInter.class);
        
        ci.printList();
    }
}
