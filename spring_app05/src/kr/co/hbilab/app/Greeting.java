package kr.co.hbilab.app;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Greeting implements Message{

    @Override
    public void sayHello() {
        Date d = new Date();
        int hour = d.getHours();
        SimpleDateFormat sdf = new SimpleDateFormat("HH시mm분ss초");
        String now = sdf.format(d);
        
        if(hour<=12){
            System.out.println("현재시간 : "+now + "\n아침입니다");
        }else if(hour<=17){
            System.out.println("현재시간 : "+now + "\n오후입니다");
        }else{
            System.out.println("현재시간 : "+now + "\n저녁입니다");
        }
    }
    
}
