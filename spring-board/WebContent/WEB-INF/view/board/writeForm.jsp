<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
th, td {
    border:1px solid black;
    berder-collapse:collapse;
}
th {
    background-color: #dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}

</style>
</head>
<body>
    <form:form action="write" method="post" commandName="dto">
        <input type="hidden" name="ip" id="ip" value="${ip }"/>
    	<table>
    		<tr>
    			<th><form:label path="writer">작성자</form:label></th>
    			<td><form:input path="writer"/></td>
    		</tr>
    		<tr>
    			<th><form:label path="title">제목</form:label></th>
    			<td><form:input path="title"/></td>
    		</tr>
    		<tr>
    			<th><form:label path="contents">내용</form:label></th>
    			<td><form:textarea cols="40" rows="7" path="contents"/></td>
    		</tr>
    		<tr>
    			<td colspan="2">
    			    <input type="submit" value="등록"/>
    			</td>
    		</tr>
    	</table>
    </form:form>
</body>
</html>