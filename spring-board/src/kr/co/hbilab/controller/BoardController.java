/**
 * 2015. 6. 11.
 * @author KDY
 */
package kr.co.hbilab.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kr.co.hbilab.dao.Dao;
import kr.co.hbilab.dto.BoardDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/board")
public class BoardController {
    
    @Autowired
    Dao dao;
    
    HttpSession session;
    
    @RequestMapping(value = "/list")
    public String boardList(Model model, HttpServletRequest req) {
        req.getSession().invalidate();
        model.addAttribute("list", dao.selectAll());
        return "/board/list";
    }
    
    @RequestMapping(value = "/write", method = RequestMethod.GET)
    public String write(Model model, HttpServletRequest req) {
        BoardDTO dto = new BoardDTO();
        model.addAttribute("dto", dto);
        model.addAttribute("ip", req.getRemoteAddr());
        return "/board/writeForm";
    }
    
    @RequestMapping(value = "/write", method = RequestMethod.POST)
    public String writeOk(@ModelAttribute("dto") BoardDTO dto) {
        int result = dao.insertOne(dto);
        if(result > 0){
            System.out.println("등록성공");
            return "redirect:/board/list";
        }else{
            System.out.println("등록실패");
            return "/board/writeForm";
        }
    }
    
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String boardView(@RequestParam("bno") int bno, Model model,
            HttpServletRequest req) {
        
        session = req.getSession();
        if(session.getAttribute("readSession") == null){
            dao.readCountAdd(bno);
        }
        session.setAttribute("readSession", "true");
        model.addAttribute("dto", dao.selectOne(bno));
        return "/board/view";
    }
    
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public String boardViewForm(@RequestParam("bno") int bno, Model model) {
        model.addAttribute("dto", dao.selectOne(bno));
        return "/board/view";
    }
    
    @RequestMapping(value = "/hits")
    public String hits(@RequestParam("bno") int bno, Model model) {
        dao.hitsCountAdd(bno);
        model.addAttribute("dto", dao.selectOne(bno));
        return "redirect:/board/view?bno=" + bno;
    }
    
}
